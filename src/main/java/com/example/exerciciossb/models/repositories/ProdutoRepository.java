package com.example.exerciciossb.models.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.example.exerciciossb.models.entities.Produto;

public interface ProdutoRepository extends PagingAndSortingRepository<Produto, Integer> {
    public Iterable<Produto> findByNomeContaining(String nome);
}

